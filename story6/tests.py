from django.test import TestCase, Client
from .models import *
from .views import *

class UnitTestStory6(TestCase):
    ##### Index #####
    def test_story6_is_exist(self):
        response = self.client.get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_story6_using_index_template(self):
        response = self.client.get('/story6/')
        self.assertTemplateUsed(response, 'story6/index.html', 'base_vanilla.html')

    
    ##### Create Kegiatan #####
    def test_story6_is_exist(self):
        response = self.client.get('/story6/create-kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_story6_using_form_kegiatan_template(self):
        response = self.client.get('/story6/create-kegiatan/')
        self.assertTemplateUsed(response, 'story6/form_kegiatan.html', 'base_vanilla.html')


    def test_can_save_kegiatan_POST_request(self):
        response = self.client.post('/story6/create-kegiatan/', data = {'nama':'Tidur cantik'})
        count_all_kegiatan = Kegiatan.objects.all().count()
        self.assertEqual(count_all_kegiatan, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/story6/')

        new_response = self.client.get('/story6/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Tidur cantik', html_response)