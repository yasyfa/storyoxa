from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = 'story6'

urlpatterns = [
    path('', views.kegiatan, name='kegiatan'),
    path('create-kegiatan/', views.createKegiatan, name='form_kegiatan'),
    path('create-peserta/<int:id>', views.createPeserta, name='form_peserta'),
]

urlpatterns += staticfiles_urlpatterns()