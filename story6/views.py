from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
from . import forms


def kegiatan(request):
    list_kegiatan = Kegiatan.objects.all()
    list_peserta = Peserta.objects.all()
    context = {
        'list_kegiatan': list_kegiatan,
        'list_peserta': list_peserta,
    }
    return render(request, 'story6/index.html', context)

def createKegiatan(request):
    form = forms.CreateKegiatan()
    if request.method == 'POST':
        form = forms.CreateKegiatan(request.POST)

        if form.is_valid():
            form.save()
            return redirect('story6:kegiatan')
    
    return render(request, 'story6/form_kegiatan.html', {'form' : form})

def createPeserta(request, id):
    form = forms.CreatePeserta()
    if request.method == 'POST':
        print(request.POST)
        kegiatan = Kegiatan.objects.get(id=id)
        nama_peserta = request.POST.get('nama')
        Peserta.objects.create(Kegiatan=kegiatan, nama=nama_peserta)
        return redirect('story6:kegiatan')

    return render(request, 'story6/form_peserta.html', {'form' : form})