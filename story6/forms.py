from django import forms
from . import models

class CreateKegiatan(forms.ModelForm):
    class Meta:
        model = models.Kegiatan
        fields = ['nama']


class CreatePeserta(forms.ModelForm):
    class Meta:
        model = models.Peserta
        fields = ['nama']