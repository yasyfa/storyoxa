from django.db import models

class Kegiatan(models.Model):
    nama = models.CharField(max_length=50)

    def __str__(self):
        return self.nama

class Peserta(models.Model):
    nama = models.CharField(max_length=50)
    Kegiatan = models.ForeignKey(Kegiatan, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.nama
