from django.urls import path

from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = 'story1'

urlpatterns = [
    path('', views.story1, name='story1'),
]

urlpatterns += staticfiles_urlpatterns()