from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def miscellaneous(request):
    return render(request, 'main/miscellaneous.html')