from django.urls import path

from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('miscellaneous/', views.miscellaneous, name='miscellaneous'),
]

urlpatterns += staticfiles_urlpatterns()