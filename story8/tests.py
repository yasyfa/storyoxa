from django.test import TestCase, Client
from .views import *

class UnitTestStory8(TestCase):
    ##### Index #####
    def test_story8_is_exist(self):
        response = self.client.get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_story8_using_index_template(self):
        response = self.client.get('/story8/')
        self.assertTemplateUsed(response, 'story8/index.html', 'base_vanilla.html')

    def test_story8_data_is_exist(self):
        response = self.client.get('/story8/data/?q=test')
        self.assertEqual(response.status_code, 200)
        content = str(response.content, encoding='utf8')
        self.assertIn('{"kind": "books#volumes",', content)