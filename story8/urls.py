from django.urls import path
from .views import *
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = 'story8'

urlpatterns = [
    path('', index, name='index'),
    path('data/', data, name='data')
]

urlpatterns += staticfiles_urlpatterns()