jQuery(document).ready(function($) {
  
    $('#sb-keyword').keyup(function(){
        var $this = $(this);
        var query = $this.val();

        $.ajax({
            url: 'data/?q=' + query,
            success: function(data) {
                var books = data.items
                var book_list = $('#blist');

                book_list.empty();
                $("#not-found").remove();

                if(typeof (data.items) === 'undefined'){
                    book_list.append('<h3 id="not-found"> No books found </h3>')
                } else {
                    for (i = 0; i < books.length; i++) {
                        var title = books[i].volumeInfo.title;
                        var desc = books[i].volumeInfo.description;
                        var link = books[i].volumeInfo.canonicalVolumeLink;

                        if(typeof(desc) === 'undefined') {
                            desc = "No description";
                        } else {
                            if (desc.length > 250) {
                                desc = desc.slice(0, 250) + "...";
                            } else {
                                desc = desc.slice(0, 250);
                            }
                        }

                        var book = '<br><h5><a class="bttl" href="' + link + '">'+ title +'</a></h5><br><p>' + desc + '</p><br>';
                        book_list.append(book);
                    }
                }
            }
        });
    });
  });