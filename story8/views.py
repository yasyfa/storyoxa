from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json

def index(request):
    return render(request, 'story8/index.html')

def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    res = requests.get(url)

    books = json.loads(res.content)
    return JsonResponse(books, safe=False)