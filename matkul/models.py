from django.db import models
from django.utils.text import slugify

class Matkul(models.Model):
    nama = models.CharField(max_length=50)
    slug = models.SlugField(editable=False)
    dosen = models.CharField(max_length=50)
    sks = models.PositiveSmallIntegerField()
    deskripsi = models.TextField(max_length=500)
    sem_th = models.CharField(max_length=20)
    ruang = models.CharField(max_length=15)

    def __str__(self):
        return self.nama 
    
    def snippet(self):
        if len(self.deskripsi) > 35:
            return self.deskripsi[:35] + '...'
        else:
            return self.deskripsi
        

    def save(self):
        super(Matkul, self).save()
        self.slug = slugify(f'{self.id}-{self.nama}')
        return super(Matkul, self).save()