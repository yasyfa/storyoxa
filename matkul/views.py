from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Matkul
from . import forms


def matkul(request):
    list_matkul = Matkul.objects.all().order_by('nama')
    return render(request, 'matkul/index.html', {'list_matkul': list_matkul})

def detail_matkul(request, slug):
    matkul =  Matkul.objects.get(slug=slug)
    return render(request, 'matkul/details.html', {'matkul':matkul})

def create_matkul(request):
    form = forms.CreateMatkul()
    if request.method == 'POST':
        form = forms.CreateMatkul(request.POST)
        if form.is_valid():
            form.save()
            return redirect('matkul:matkul')    

    return render(request, 'matkul/create_matkul.html', {'form':form})

def update_matkul(request, id):
    matkul = Matkul.objects.get(id=id)
    form = forms.CreateMatkul(instance=matkul)
    if request.method == 'POST':
        form = forms.CreateMatkul(request.POST, instance=matkul)
        if form.is_valid():
            form.save()
            return redirect('matkul:matkul')

    return render(request, 'matkul/create_matkul.html', {'form':form})

def delete_matkul(request, id):
    Matkul.objects.get(id=id).delete()
    return redirect('matkul:matkul')


