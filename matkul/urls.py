from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = 'matkul'

urlpatterns = [
    path('', views.matkul, name='matkul'),
    path('<slug:slug>', views.detail_matkul, name='detail'),
    path('create-matkul/', views.create_matkul, name='create_matkul'),
    path('delete/<int:id>', views.delete_matkul, name='delete_matkul'),
    path('update/<int:id>', views.update_matkul, name='update_matkul'),
]

urlpatterns += staticfiles_urlpatterns()