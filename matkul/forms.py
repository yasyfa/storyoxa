from django import forms
from . import models

class CreateMatkul(forms.ModelForm):
    class Meta:
        model = models.Matkul
        fields = ['nama', 'dosen', 'sks', 'sem_th', 'ruang', 'deskripsi']
        