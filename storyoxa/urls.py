from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls')),
    path('story1/', include('story1.urls')),
    path('matkul/', include('matkul.urls')),
    path('story6/', include('story6.urls')),
    path('story7/', include('story7.urls')),
    path('story8/', include('story8.urls')),
    path('story9/', include('story9.urls')),
]
