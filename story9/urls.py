from django.urls import path
from .views import *
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = 'story9'

urlpatterns = [
    path('', index, name='index'),
    path('register/', register_views, name='register'),
    path('login/', login_views, name='login'),
    path('logout/', logged_out, name='logout'),
    
]

urlpatterns += staticfiles_urlpatterns()