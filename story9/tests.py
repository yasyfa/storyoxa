from django.test import TestCase, Client
from django.contrib.auth.models import User
from .views import *

class UnitTestStory9(TestCase):
    def setUp(self):
        self.credentials = {
            'username':'tst',
            'password':'iAmyOur67tsT43pAsSwrd'
        }
        User.objects.create_user(**self.credentials)
    
    def test_story9_will_redirect_if_not_logged_on(self):
        response = self.client.get('/story9/')
        self.assertEqual(response.status_code, 302)

    def test_story9_register_and_index(self):
        response = self.client.get('/story9/register/')
        self.assertEqual(response.status_code, 200)
        data = {
            'username': 'Test',
            'first_name':'test',
            'last_name' : 'user',
            'password1' : 'iAmyOur67tsT43pAsSwrd',
            'password2' : 'iAmyOur67tsT43pAsSwrd'
        }
        response = self.client.post('/story9/register/', data)
        self.assertEqual(response.status_code, 302)

    def test_story9_logout(self):
        self.client.login(username='tst', password='iAmyOur67tsT43pAsSwrd')
        response = self.client.post('/story9/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual (response['location'], '/story9/logout/')


    def test_story9_login(self):
        response = self.client.get('/story9/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/login.html', 'base_vanilla.html')
        data = {
            'username': 'tst',
            'password' : 'iAmyOur67tsT43pAsSwrd'
        }
        response2 = self.client.post('/story9/login/', data)
        self.assertEqual(response2.status_code, 302)

    def test_index_if_logged_in(self):
        self.client.login(username='tst', password='iAmyOur67tsT43pAsSwrd')
        response = self.client.get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_story9_logged_out_page_exist(self):
        response = self.client.get('/story9/logout/')
        self.assertEqual(response.status_code, 200)
