from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import login_required
from .forms import *

@login_required(login_url='story9:login')
def index(request):
    if request.method == 'POST':
        logout(request)
        return redirect('story9:logout')
    else:
        user = request.user
        context = {
            'user':user,
        }
        return render(request, 'story9/index.html', context)

def register_views(request):
    if request.method == 'POST':
        form = CreateUser(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('story9:index')
    form = CreateUser()
    context = {
        'form':form,
    }
    return render(request, 'story9/register.html', context)

def login_views(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('story9:index')
    
    form = AuthenticationForm()
    context = {
        'form':form,
    }
    return render(request, 'story9/login.html', context)

def logged_out(request):
    return render(request, 'story9/logout.html')