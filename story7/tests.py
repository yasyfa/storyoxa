from django.test import TestCase

from django.test import TestCase, Client
from .views import *

class UnitTestStory7(TestCase):
    ##### Index #####
    def test_story7_is_exist(self):
        response = self.client.get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_story7_using_index_template(self):
        response = self.client.get('/story7/')
        self.assertTemplateUsed(response, 'story7/index.html', 'base_vanilla.html')