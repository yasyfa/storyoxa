jQuery(document).ready(function($) {

  $('.acc-btn').click(function() {
    var $this = $(this);
    var panel = $this.parent().next().children();

    if (panel.is(':visible')) {
      panel.slideUp();
    } else {
      panel.slideDown();
    }
  });

  $('.up').click(function() {
    var $this = $(this);
    var accordion = $this.closest('.accordion');

    accordion.insertBefore(accordion.prev());
  });

  $('.down').click(function() {
    var $this = $(this);
    var accordion = $this.closest('.accordion');

    accordion.insertAfter(accordion.next());
  });

});
